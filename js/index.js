const usernameSubmit = document.querySelector('#usernameSubmit');
const questionApiClient = new QuestionApiClient();

let username;

const selectedQuestions = [];

let counter = 0;
let maxRounds = 5;

const questionPoints = 100;
let score = 0;

let timeleft = 10;
let timer = setInterval(displayTimer, 1000);


usernameSubmit.addEventListener('click', handleUsernameEntered);


function displayTimer() {
    const timerElement = document.querySelector('#timer');

    if(!timerElement) {
        clearInterval(timer);
        return;
    }

    timerElement.innerText = timeleft + ' seconds left';

    if(timeleft <= 0){
        clearInterval(timer);
        nextQuestion();
    }

    if(timeleft === 0) {
        timeleft = 10;
    }
    else {
        timeleft--;
    }
};


function addUsernameError(element) {
    element.classList.add('is-invalid');    
}

function removeUsernameError(element) {
    element.classList.remove('is-invalid');
}


function addAnswerError() {
    const error = document.querySelector('#answerError');
    error.innerHTML += `<div class="alert alert-danger">
     Pls select a answer
  </div>`;
}


function removeAnswerError() {
    document.querySelector('#answerError').innerHTML = '';
}


function displayScore() {
    username = username.charAt(0).toUpperCase() + username.slice(1);

    document.querySelector('main>div').innerHTML = `
    <div>
        <p>${username}: ${score} Points</p>
    <div>
    `;
}


function displayQuestion(question) {
    
    if(counter > maxRounds) {
        return;
    }
    
    let htmlString = `
    <div>
        <p>${question.text}</p>
    </div>

    <div class="form-group">`;

    htmlString += question.antworten.map( (answer, key) => {
        return `
        <div class="form-check">
            <label for="${key}" class="form-check-label">${answer}</label>
            <input type="radio" name="answer" value="${key}" id="${key}" class="form-check-input">
        </div>
        `;
    }).join('');

    htmlString += `
        <div id='answerError' class="my-2">
        </div>
    </div>

    <button class="btn btn-primary" id="answerButton" onclick="handleAnswerQuestion()">antworten</button>
    <div>
        <span id="timer"></span>
    </div>
   
    <span>${counter} / ${maxRounds}</span>
    `;

    document.querySelector('main>div').innerHTML = htmlString;
    ;

    timer = setInterval(displayTimer, 1000);
}



function getRandomNumber(max) {
    return Math.floor(Math.random() * max);
}


async function nextQuestion(questonCount = null) {
    counter++;

    if(counter > maxRounds) {
        displayScore();
        return;
    }

    if(questonCount === null) {
        const questonCountResponse = await questionApiClient.fetchQuestionCount();
        questonCount = await questonCountResponse.max;
    }

    const randomNumber = getRandomNumber(questonCount);

    if(selectedQuestions.includes(randomNumber)) {
        nextQuestion(questonCount);
    }

    var questionResponse = await questionApiClient.fetchQuestion(randomNumber);

    selectedQuestions.push(randomNumber);

    displayQuestion(await questionResponse);
}


async function chechAnswer(answer) {
    if(isNaN(answer)) {
        return;
    }

    const currentQuestionID = selectedQuestions.slice(-1);
    
    const correctAnswer = await questionApiClient.fetchCorrectAnswer(currentQuestionID);
    
    if(answer === correctAnswer.correct) {
        score = score + questionPoints;
    }
    else {
        score = score - questionPoints;
        if(score <= 0) {
            score = 0;
        }
    }
}

function handleAnswerQuestion() {
    const userInput = document.querySelector('input[name="answer"]:checked');

    removeAnswerError();
    
    if(!userInput) {
        addAnswerError();
        return;
    }

    document.querySelector('#answerButton').disabled = true;
    
    chechAnswer(Number(userInput.value));

    clearInterval(timer);
    timeleft = 10;

    nextQuestion();
} 

function handleUsernameEntered() {
    const usernameInput = document.querySelector('#username');

    removeUsernameError(usernameInput);

    if(usernameInput.value === '' || !isNaN(Number(usernameInput.value))) {
        addUsernameError(usernameInput);
        return;
    }

    username = usernameInput.value;
    
    nextQuestion();
}





