class QuestionApiClient {

    constructor() {
        this.url = "http://wifi.1av.at/quiz.php";
    }

    async fetchQuestionCount() {
        try {
            const response = await fetch(this.url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body:  new URLSearchParams({
                    'type': 'getcount'
                })
            });

            this.checkResponse(response);

            const result = await response.json();
            return result

        } catch(error) {
            //500er error
            console.log(error);
        }
    }

    async fetchQuestion(id) {
        try {
            const response = await fetch(this.url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body:  new URLSearchParams({
                    'type': 'getquestion',
                    'id' : id
                })
            });

            this.checkResponse(response);

            const result =  await response.json();
            return result;


        } catch(error) {
            //500er error
            console.log(error);
        }
    }

    async fetchCorrectAnswer(id) {
        try {
            const response = await fetch(this.url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body:  new URLSearchParams({
                    'type': 'check',
                    'id' : id
                })
            });

            this.checkResponse(response);

            const result =  await response.json();
            return result;
        } catch(error) {
            //500er error
            console.log(error);
        }
    }

    checkResponse(response) {
        if(response.status !== 200) {
            console.log('error');
            console.log(response.status);
        }
    }

}